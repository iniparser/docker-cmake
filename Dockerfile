FROM gcc:latest

LABEL MAINTAINERS="Lars Möllendorf <lars@moellendorf.eu>"

RUN echo 'deb http://deb.debian.org/debian bookworm-backports main' > /etc/apt/sources.list.d/backports.list

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
    cmake/bookworm-backports \
    extra-cmake-modules \
    doxygen \
    tree \
    valgrind \
    ruby \
    ruby-dev

